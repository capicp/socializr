
import sys, os

if 'default' not in sys.path:
    sys.path[0:0] = ['default']

from routes import ROUTES

from google.appengine.ext import webapp

app_config = {
  'webapp2_extras.sessions': {
  'secret_key': '5586c23l'
  },
  'webapp2_extras.auth': {
    'user_model': 'models.User',
    'user_attributes': ['email']
  }
}

debug_mode = False

if 'Development' in os.environ['SERVER_SOFTWARE']:
  debug_mode = True

app = webapp.WSGIApplication(ROUTES, config=app_config, debug=debug_mode)

import requests
import requests_toolbelt.adapters.appengine

requests_toolbelt.adapters.appengine.monkeypatch()
