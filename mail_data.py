#!/usr/bin/env python
# -*- coding: utf-8 -*-

mail_templates = {
    'account_expired': {
        'title_mail': u'¡Se ha agotado el tiempo de suscripción!',
        'greeting': u'Hola %(name)s',
        'body': u'Lamentamos informarte que el tiempo de suscripción para tu cuenta @%(screen_name)s se ha agotado. Si quieres seguir disfrutando de nuestros servicios te invitamos a extender tu suscripción.',
        'button_url': '%(server)s/editor/%(screen_name)s/extend',
        'button_text': u'Extender suscripción',
        'server_url': '%(server)s'
    },
    'account_will_expire':{
        'title_mail': u'Faltan pocos días para el vencimiento de tu suscripción',
        'greeting': u'Hola %(name)s',
        'body': u'Te recordamos que ya falta muy poco para que el tiempo de suscripción de tu cuenta @%(screen_name)s se agote. Si quieres seguir disfrutando de nuestros servicios te invitamos a extender tu suscripción.',
        'button_url': '%(server)s/editor/%(screen_name)s/extend',
        'button_text': u'Extender suscripción',
        'server_url': '%(server)s'
    },
    'payment_successful':{
        'title_mail': u'Suscripción extendida',
        'greeting': u'Hola %(name)s',
        'body': u'Su pago ha sido procesado con éxito, por lo que dispone de 30 días más de suscripción para su cuenta @%(screen_name)s.',
        'button_url': '%(server)s/editor/%(screen_name)s/dashboard',
        'button_text': u'Ir al dashboard',
        'server_url': '%(server)s'
    }
}