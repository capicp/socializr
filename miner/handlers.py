#encoding:utf-8
'''
Created on 08/01/2016

@author: capi
'''

'''
Created on 24/12/2015

@author: capi
'''

from datetime import date, datetime, timedelta
import logging
import random
import time

from google.appengine.ext import deferred
from google.appengine.ext import ndb
import webapp2

import configs
from miner_core import TargetSeeker
from models import TwitterAccount, FollowersHistoric, EngagementSession, \
    EngageAction
from secrets import TWITTERAPP_CONSUMER_DATA
import secrets
from tasks import ClearInteractionsTask, EngageAccountTask
from twitterapi.clients import TwitterRESTClient
from twitterapi.objects import ApiError
from twitterapi.utils import raise_api_error


class MinerBaseHandler(webapp2.RequestHandler):

    account = None

    # Suma de segundos transcurridos entre acciones
    seconds_sum = 0

    def init_client(self):

        consumer_data = {
                         'KEY': secrets.TWITTERAPP_CONSUMER_DATA['KEY'],
                         'SECRET': secrets.TWITTERAPP_CONSUMER_DATA['SECRET']
        }

        token_data = {
                      'KEY': self.account.token.key,
                    'SECRET': self.account.token.secret
                      }

        self.client = TwitterRESTClient(consumer_data=consumer_data, token_data=token_data)


    def seconds_for_next_action(self):

        if (self.seconds_sum == 0):
            seconds_interval = random.randint(0, configs.MAXHOURS_INTERVAL_INIT_ENGAGEMENT*60*60)
        else:
            seconds_interval = random.randint(configs.MINSECONDS_INTERVAL_ACTIONS, configs.MAXSECONDS_INTERVAL_ACTIONS)

        self.seconds_sum = self.seconds_sum + seconds_interval

        return self.seconds_sum


class PrepareEngagementTasks(webapp2.RequestHandler):

    def get(self):

        accounts = TwitterAccount.get_active_accounts()

        for account in accounts:

            account_key = account.key.urlsafe()

            EngageAccountTask(account_key).add()


class EngagementTaskHandler(MinerBaseHandler):

    client = None

    def _engage_audience(self, audience, users_exclude):

        engage_session = EngagementSession(parent=self.account.key)

        seeker = TargetSeeker(query=audience.search_query, client=self.client)

        follows= []
        likes = []
        last_tweet = None

        # Aunque es un parametro con valor por defecto, si no se pasa user_ids a veces tiene los ids de la ejecucion anterior. Weird


        try:
            seeker.search(users_exclude, count=audience.follows_quota + audience.likes_quota, lang=self.account.language, locale=self.account.language)
        except ApiError as err:
            logging.error(err)
            return None

        tweets = seeker.get_target_tweets()

        if len(tweets) == 0:
            logging.info("No se encontraron tweets para la audiencia")
            return None


        logging.info("Found: " + str(len(tweets)))
        logging.info("Follows Quotas: " + str(audience.follows_quota))
        logging.info("Favs Quotas:" + str(audience.likes_quota))

        n_follows = audience.follows_quota

        # Se verifica si tenemos suficientes tweets para las cuotas
        if len(tweets) < audience.follows_quota:
            logging.info("No hemos conseguido suficiente tweets para cumplir la cuota")

            if len(tweets) < audience.likes_quota:
                n_follows = len(tweets) / 2 # TODO: Quizás debería ser la proporcion entre quota para seguir y favs
            else:
                n_follows = audience.follows_quota - audience.likes_quota
        ###

        tweets_follow = []
        for i in range(0,n_follows):

            try:
                tweets_follow.append(tweets.pop(i))
            except IndexError as err:
                logging.error(err) # Revisar por que se puede dar este error


        follows = self._follow_users(audience, tweets_follow)

#         for t in tweets:
#             print t.get_url()

        if len(tweets) > 0:

            likes = self._like_tweets(audience, tweets)

#         for t in new_tweets:
#             print t.get_url()

        actions = follows + likes
        engage_session.actions = actions

        return engage_session

    def get_users_excluded(self):
        last_engage_sessions = EngagementSession.query(EngagementSession.realized_at >= (datetime.now() - timedelta(days=5)) and EngagementSession.realized_at < datetime.now()).fetch()        
        users_exclude = set([])
        
        for last_session in last_engage_sessions:
            actions = last_session.actions
            for action in actions:
                users_exclude.add(action.user_target_id)
        
        return users_exclude

    def _follow_users(self, audience, tweets):

        actions = []

        for tweet in tweets:

            if not tweet.user.following:

                task_name = ("ef_%s_%d_%d" % (self.account.screen_name, tweet.user.id, int(time.time())) )

                countdown = self.seconds_for_next_action()

                deferred.defer(self.client.follow, tweet.user.id, 
                    _countdown=countdown, 
                    _name=task_name)

                task_name = ("em_%s_%d_%d" % (self.account.screen_name, tweet.user.id, int(time.time())) )

                
                deferred.defer(self.client.mute, tweet.user.id, 
                    _countdown=countdown+3, 
                    _name=task_name)

                #result = self.client.follow(tweet.user.id)
                #audience.add_interaction(tweet.user)

                action = EngageAction(user_target_id = tweet.user.id)
                action.set_as_follow()

                actions.append(action)

        return actions


    def _like_tweets(self, audience, tweets):

        actions = []

        for tweet in tweets:

            if not tweet.favorited and not tweet.user.following:

                #self.random_wait()
                task_name = ("el_%s_%d_%d" % (self.account.screen_name, tweet.id, int(time.time())) )

                deferred.defer(self.client.like, tweet.id, _countdown=self.seconds_for_next_action(), _name=task_name)

                #audience.add_interaction(tweet)

                action = EngageAction(user_target_id=tweet.user.id, status_target_id=tweet.id)
                action.set_as_like()

                actions.append(action)

        return actions

    def _audience_has_enough_data(self, audience):

        return audience.get_successful_rate() > 0

    def _get_random_cuota(self, max_cuota):

        return random.randint(max_cuota-configs.QUOTA_MAX_DEVIATION, max_cuota )

    def _assign_quotas(self, audiences, follows_max_cuota, likes_max_cuota):

        success_rates_sum = 0
        smaller_successful_rate = 100

        for audience in audiences:
            if audience.get_successful_rate() > 0 and audience.get_successful_rate() < smaller_successful_rate:
                smaller_successful_rate = audience.get_successful_rate()

        # Se asigna un campo quote_successful_rate el cual se usara para el calculo de de la cuota
        for audience in audiences:

            if not self._audience_has_enough_data(audience):
                audience.quota_successful_rate = smaller_successful_rate
            else:
                audience.quota_successful_rate = audience.get_successful_rate()

            success_rates_sum = success_rates_sum + audience.quota_successful_rate


        for audience in audiences:

            quota_rate = ((audience.quota_successful_rate*100) / success_rates_sum)*0.01

            audience.follows_quota = int(round(follows_max_cuota * quota_rate))
            audience.likes_quota = int(round(likes_max_cuota * quota_rate))

            # TODO: audiences with 0 quota

            logging.info("-- Para audiencia " + audience.search_query)
            logging.info("----quota_successful_rate: " + str(audience.quota_successful_rate))
            logging.info("----quota_rate: " + str(quota_rate))
            logging.info("----follows_quota: " + str(audience.follows_quota))
            logging.info("----likes_quota: " + str(audience.likes_quota))


        return audiences


    # Main Methdd

    def post(self):

        # TODO: Check for credentials

        account_key = self.request.get('account_key')

        self.account = ndb.Key(urlsafe=account_key).get()

        self.init_client()

        logging.info('Engagement on account %s started' %(self.account.screen_name))

        if self.request.get('only_news', False):
            audiences = self.account.get_new_audiences()
        else:
            audiences = self.account.get_active_audiences()

        sessions_keys = []

        self._assign_quotas(audiences,
                            follows_max_cuota= self._get_random_cuota(configs.FOLLOWS_PER_AUDIENCE),
                            likes_max_cuota=self._get_random_cuota(configs.LIKES_PER_AUDIENCE))

        users_exclude = self.get_users_excluded()

        for a in audiences:

            logging.info('Se inicia engagement con audiencia %s' % (a.search_query))

            engage_session = self._engage_audience(a, users_exclude)

            if (engage_session):
                session_key = a.add_engage_session(engage_session)
                sessions_keys.append(session_key.urlsafe())

        if len(sessions_keys) > 0:
            cleaner_task = ClearInteractionsTask(sessions_keys=sessions_keys)
            cleaner_task.add()

        logging.info('Engagement on account %s finished' %(self.account.screen_name))


class CleanAccountTaskHandler(MinerBaseHandler):

    def get_friendships(self, interactions):

        PARTITION_SIZE = 50

        total_api_requests = (len(interactions) / PARTITION_SIZE) + 1 # + 1 en caso de que no se una division exacta, si es inncesaria es detenida por e break del ciclo

        friendships = []

        for n_request in range(0,total_api_requests):

            index_from = n_request*PARTITION_SIZE
            index_to = n_request*PARTITION_SIZE + PARTITION_SIZE

            ids_array = []

            for i in range(index_from, index_to):

                try:
                    ids_array.append(interactions[i].user_target_id)
                except IndexError:
                    break

            logging.debug(ids_array)

            users = self.client.get_friendships(ids_array).users

            friendships = friendships + users

        return friendships

    def user_for(self, users, user_id):

        for user in users:
            if user.id == user_id:
                return user

        logging.error("user %d no encontrado " % user_id)

    def post(self):

        # All the sessions keys to clear
        sessions_keys = self.request.POST.values()

        # Arrays of entities for batch put
        cleaned_sessions = []
        cleaned_audiences = []

        # Sessions keys unsuccesful clean
        deferred_sessions_keys = []

        # Array of integers ids for ensure not count various successful actions on the same follower
        new_followers_ids = []

        for session_key in sessions_keys:

            session = ndb.Key(urlsafe=session_key).get()

            audience = session.get_audience()

            if not self.account or (self.account.key.id() != audience.get_account_key().id()):
                self.account = audience.get_account_key().get()

            self.init_client()

            actions = [action for action in session.actions if not action.cleaned]

            try:
                users = self.get_friendships(actions)
            except ApiError as error:
                deferred_sessions_keys.append(session_key)
                logging.warn("Session clean deferred")
                logging.warn(error)
                continue

            logging.info('Clean for audience %s on account %s started' %(audience.search_query, self.account.screen_name))

            cleaned_actions = []

            for action in actions:

                target_user = self.user_for(users, action.user_target_id)
                audience.n_interactions = audience.n_interactions + 1

                if target_user and target_user.is_friend():
                    if target_user.id not in new_followers_ids:

                        audience.n_success = audience.n_success + 1
                        action.success = True
                        action.cleaned = True

                        new_followers_ids.append(target_user.id)

                else:
                    if action.is_follow():
                        next_action_seconds = self.seconds_for_next_action()

                        unfollow_task_name = ("cf_%s_%d_%d" % (self.account.screen_name, action.user_target_id, int(time.time())))
                        deferred.defer(self.client.unfollow, 
                            id=action.user_target_id, 
                            _countdown=self.seconds_for_next_action(), 
                            _name=unfollow_task_name, retry_count=5)
                        unmute_task_name = ("cm_%s_%d_%d" % (self.account.screen_name, action.user_target_id, int(time.time())))
                        deferred.defer(self.client.unmute, 
                            id=action.user_target_id, 
                            _countdown=next_action_seconds+5, 
                            _name=unmute_task_name, retry_count=5)
                        
                            
                if action.is_like():
                    task_name = ("cl_%s_%d_%d" % (self.account.screen_name, action.user_target_id, int(time.time())))
                    deferred.defer(self.client.unlike, 
                    id=action.status_target_id, 
                    _countdown=self.seconds_for_next_action(), 
                    _name=task_name, retry_count=5)
                    

                cleaned_actions.append(action)

            cleaned_audiences.append(audience)

            if len(cleaned_actions) == len(session.actions):
                session.cleaned = True
                cleaned_sessions.append(session)

            logging.info('Clean on session finished')

        # We save all the models in one batch put
        ndb.put_multi(cleaned_sessions + cleaned_audiences)

        if len(deferred_sessions_keys) > 0:
            cleaner_task = ClearInteractionsTask(sessions_keys=deferred_sessions_keys) # TODO: deberia ejecutarse poco tiempo despues
            cleaner_task.add()
            



class SaveFollowersHistoric(MinerBaseHandler):

    def save_all_accounts(self):

        accounts = TwitterAccount.get_active_accounts()

        day = datetime.now() - timedelta(days=1)
        
        for a in accounts:

            self.account = a
            self.init_client()

            try:
                user = self.client.get_user(a.user_id)

                if (user is not None):
                    followers = FollowersHistoric(count=user.followers_count,parent=self.account.key, day=day)
                    followers.put()

            except ApiError as err:
                logging.warn(a.screen_name)
                logging.warn(err)

# Utils

class TestCall(MinerBaseHandler):

    def call_raw_response(self):

        import json

        account_key = self.request.get('account_key')

        self.account = ndb.Key(urlsafe=account_key).get()

        self.init_client()

        result = self.client.get_friendships(screen_names=['capicp', 'marelynaguilera'])

#         for user in result:
#
#             if user.is_friend():
#                 print user.screen_name + " es amigo"
#             else:
#                 print user.screen_name + " no es amigo"


        self.response.headers['Content-Type'] = 'application/json'

        self.response.write(json.dumps(result))
