
'''
Created on 28/12/2015

@author: capi
'''
from datetime import timedelta
import os


FOLLOWS_PER_AUDIENCE = 40
LIKES_PER_AUDIENCE = 120
TIME_FOR_CLEAN = timedelta(hours=48)

MINSECONDS_INTERVAL_ACTIONS = 30
MAXSECONDS_INTERVAL_ACTIONS = 120

# Maximo cantidad de una cuota que puede ser descontada de forma aleatoria
QUOTA_MAX_DEVIATION = 9

# Intervalo de tiempo en el que se puede iniciar un proceso de acciones (engagement / clean) a una cuenta
MAXHOURS_INTERVAL_INIT_ENGAGEMENT = 3

if 'Development' in os.environ['SERVER_SOFTWARE']:

    # We are in development

    FOLLOWS_PER_AUDIENCE = 5
    LIKES_PER_AUDIENCE = 5
    QUOTA_MAX_DEVIATION = 3
    TIME_FOR_CLEAN = timedelta(minutes=5)
    MINSECONDS_INTERVAL_ACTIONS = 5
    MAXSECONDS_INTERVAL_ACTIONS = 15

