'''
Created on 08/01/2016

@author: capi
'''

from webapp2 import Route


ROUTES = [

        Route('/tasks/engageAccount', handler='handlers.EngagementTaskHandler', name="engage_account"),
        Route('/tasks/cleanAccount', handler='handlers.CleanAccountTaskHandler', name="clear_account"),

        Route('/tasks/saveFollowersHistoric', handler='handlers.SaveFollowersHistoric:save_all_accounts', name="save_followers_historic"),
        Route('/tasks/prepareEngagementTasks', handler='handlers.PrepareEngagementTasks', name="task__prepare_engagement"),

        Route('/tasks/testCallRaw', handler='handlers.TestCall:call_raw_response', name="call_raw_response"),

]