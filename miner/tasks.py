'''
Created on 24/12/2015

@author: capi
'''

from datetime import datetime

from google.appengine.api.taskqueue import Task
from webapp2 import uri_for

import configs


class EngageAccountTask(Task):

    def __init__(self, account_key):

        method = "POST"
        url = uri_for('engage_account')

        params = {}
        params['account_key'] = account_key

        super(EngageAccountTask, self).__init__(url=url, method=method, params=params)

    def add(self, queue_name='engagement', transactional=False):

        super(EngageAccountTask, self).add(queue_name=queue_name, transactional=transactional)

class ClearInteractionsTask(Task):

    def __init__(self, sessions_keys): # TODO: this can change to interactions

        method = 'POST'
        url = uri_for('clear_account')

        eta = datetime.now() + configs.TIME_FOR_CLEAN

        params = {}

        for i in range(0, len(sessions_keys)):
            params['sessions_key_' + str(i)] = sessions_keys[i]

        super(ClearInteractionsTask, self).__init__(url=url, method=method, params=params, eta=eta)

    def add(self, queue_name='cleaners', transactional=False):

        super(ClearInteractionsTask, self).add(queue_name=queue_name, transactional=transactional)









