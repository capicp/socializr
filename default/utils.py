from google.appengine.api import users

def user_required(handler):
  """
    Decorator that checks if there's a user associated with the current session.
    Will also fail if there's no session present.
  """
  def check_login(self, *args, **kwargs):
    current_user = users.get_current_user()

    auth = self.auth
    if not auth.get_user_by_session() and not users.is_current_user_admin():
      url_params = url_to_continue_page(self.request.url)

      self.redirect(self.uri_for('login') + url_params, abort=True)
    else:
      return handler(self, *args, **kwargs)

  return check_login

def admin_required(handler):
  """
    Decorator that checks if there's a user associated with the admin account.
  """

  def check_access(self, *args, **kwargs):
      user = users.get_current_user()
      if not (user and users.is_current_user_admin()):
        url = users.create_login_url(self.request.uri)
        self.redirect(url)
      else:
          return handler(self, *args, **kwargs)
        
  return check_access



def to_vef_format(num):
    return ('{:,}'.format(num)).replace(',','.')

def url_to_continue_page(url_request):
  url_split = (url_request[(url_request.find('/editor/') + 8):]).split('/', 1)      
  url_params = '?continue=%s&screen_name=%s' % (url_split[1], url_split[0])
  return '?continue=%s&screen_name=%s' % (url_split[1], url_split[0])
