#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import date
import webapp2
from models import TwitterAccount
from google.appengine.ext import ndb

class ExpiredAccountHandler(webapp2.RequestHandler):
    def get(self, screen_name):
        self._change_due_date(screen_name)

    def _change_due_date(self, screen_name):
        
        account = TwitterAccount.query(TwitterAccount.screen_name == screen_name).get()
        
        if account:
            initial_due_date = account.due_date
            account.due_date = date.today()
            account.put()

            self.response.write('Actualizada fecha de vencimiento de cuenta %s de %s a %s' % (screen_name, initial_due_date, account.due_date))
        else:
            self.response.write('Cuenta %s no encontrada' % (screen_name))