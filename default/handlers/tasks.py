#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import date, timedelta
import webapp2
from webapp2_extras import jinja2
from models import TwitterAccount
from mail_utils import send_mail, get_public_host
from mail_data import mail_templates

from google.appengine.ext import ndb

import logging

class SendMailExpiredAccountHandler(webapp2.RequestHandler):

    def get(self):

        logging.info("Server date: %s" % (date.today()))
    
        self._send_mail_user(self._get_inactive_accounts(), 'account_expired')
        self._send_mail_user(self._get_due_to_expire_accounts(), 'account_will_expire')

    def _send_mail_user(self, accounts, data_template):
        mail_template = mail_templates[data_template]
        
        for account in accounts:

            user_email = account.get_user().email

            custom_data = dict(mail_template)
            custom_data['server_url'] = get_public_host() if get_public_host() is not None else self.request.host_url

            logging.info("Sending email to %s with account %s  with due date %s" % (user_email, account.screen_name, account.due_date))
            
            custom_data['body'] = custom_data['body'] % \
            {'screen_name': account.screen_name}

            custom_data['greeting'] = custom_data['greeting'] % \
            {'name': account.name}

            custom_data['button_url'] = custom_data['button_url'] % \
            {'server':custom_data['server_url'], 'screen_name': account.screen_name}

            try:
            
                send_mail(jinja2.get_jinja2(app=self.app), user_email, custom_data['title_mail'], custom_data)
            except Exception as e:
                logging.exception("Error sending email")

    def _get_inactive_accounts(self):
        return TwitterAccount.query(TwitterAccount.due_date >= date.today(), 
            ndb.AND(TwitterAccount.due_date <= (date.today() + timedelta(hours=23, minutes=59)))
            ).fetch()

    def _get_due_to_expire_accounts(self):
        return TwitterAccount.query(TwitterAccount.due_date >= (date.today() + timedelta(days=4)),
            ndb.AND(TwitterAccount.due_date <= (date.today() + timedelta(days=4,hours=23, minutes=59)))
            ).fetch()
