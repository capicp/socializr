'''
Created on 24/12/2015

@author: capi
'''
import logging
import os

from google.appengine.api import users
from jinja2.runtime import TemplateNotFound
from webapp2 import uri_for
import webapp2
from google.appengine.ext import ndb
from webapp2_extras import jinja2, auth, sessions

from models import TwitterAccount
from utils import user_required

class BaseRequestHandler(webapp2.RequestHandler):

  @webapp2.cached_property
  def google_user(self):
    """Shortcut to access the auth instance as a property."""
    return users.get_current_user()

  @webapp2.cached_property
  def auth(self):
    """Shortcut to access the auth instance as a property."""
    return auth.get_auth()

  @webapp2.cached_property
  def user_info(self):
    """Shortcut to access a subset of the user attributes that are stored
    in the session.

    The list of attributes to store in the session is specified in
      config['webapp2_extras.auth']['user_attributes'].
    :returns
      A dictionary with most user information
    """
    return self.auth.get_user_by_session()

  @webapp2.cached_property
  def user(self):
    """Shortcut to access the current logged in user.

    Unlike user_info, it fetches information from the persistence layer and
    returns an instance of the underlying model.

    :returns
      The instance of the user model associated to the logged in user.
    """
    u = self.user_info
    return self.user_model.get_by_id(u['user_id']) if u else None

  @webapp2.cached_property
  def user_model(self):
    """Returns the implementation of the user model.

    It is consistent with config['webapp2_extras.auth']['user_model'], if set.
    """
    return self.auth.store.user_model

  @webapp2.cached_property
  def session(self):
      """Shortcut to access the current session."""
      return self.session_store.get_session(backend="datastore")


  def display_message(self, message):
    """Utility function to display a template with a simple message."""
    params = {
      'message': message
    }
    self.render('message.html', params)

  # this is needed for webapp2 sessions to work
  def dispatch(self):
      # Get a session store for this request.
      self.session_store = sessions.get_store(request=self.request)
      
      try:
          # Dispatch the request.
          webapp2.RequestHandler.dispatch(self)
      finally:
          # Save all sessions.
          self.session_store.save_sessions(self.response)

  @webapp2.cached_property
  def jinja2(self):
    """Returns a Jinja2 renderer cached in the app registry"""
    return jinja2.get_jinja2(app=self.app)

  def render(self, template_name, template_vars={}):
    # Preset values for the template
    values = {}

    # Add manually supplied template values
    values.update(template_vars)

    # read the template or 404.html
    try:
      self.response.write(self.jinja2.render_template(template_name, **values))
    except TemplateNotFound:
      self.abort(404)

class AccountManagerHandler(BaseRequestHandler):

    account_selected = None

    def get_selected_account(self):
      return self.account_selected

    @user_required
    def dispatch(self):
              
        screen_name = self.request.route_kwargs['screen_name']

        try:
          self.account_selected = TwitterAccount.query(ancestor=self.user.key).filter(TwitterAccount.screen_name == screen_name).get()
        except Exception as ex:
          current_user = users.get_current_user()

          if current_user and users.is_current_user_admin():
            self.account_selected = TwitterAccount.query().filter(TwitterAccount.screen_name == screen_name).get()
            self.user = self.account_selected.get_user()

        for account in self.user.get_accounts():
          
          account.url_dashboard = uri_for('dashboard', screen_name=account.screen_name)

        super(AccountManagerHandler, self).dispatch()


    def account_sections_urls(self):

        screen_name = self.account_selected.screen_name

        return {
                'audiences_url': uri_for('audiences', screen_name=screen_name),
                'dashboard_url': uri_for('dashboard', screen_name=screen_name),
                'payments_url': uri_for('payments', screen_name=screen_name),
                'endpoints_host': os.environ['ENDPOINTS_HOST'] # Maybe is a better idea use a jinja custom tag for inject this
                }

    def render(self, template_name, template_vars={}):

        template_vars.update({'account_selected': self.account_selected,
                              #'logout_url': users.create_logout_url(uri_for('login_form')),
                              'logout_url': uri_for('logout'),
                              'user': self.user})

        if self.google_user:
            template_vars['logout_url'] = users.create_logout_url(uri_for('logout'))
        else:
            template_vars['logout_url'] = uri_for('logout')

        template_vars.update(self.account_sections_urls())

        super(AccountManagerHandler, self).render(template_name, template_vars)
