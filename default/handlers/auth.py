'''
Created on 24/12/2015

@author: capi
'''

from datetime import timedelta, datetime

from google.appengine.api import users
from google.appengine.ext import ndb
import webapp2
from webapp2_extras import sessions

from handlers.abstracts import BaseRequestHandler
from models import TwitterAccount, OAuthToken, User, FollowersHistoric, ExtensionTicket
from secrets import TWITTERAPP_CONSUMER_DATA
from simpleauth import SimpleAuthHandler, AuthProviderResponseError
import twitterapi.objects as twitter


class TwitterAuthHandler(SimpleAuthHandler, BaseRequestHandler):
    """
        Autenticacion por twitter
    """

    session_store = None

    def dispatch(self):
    # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        except AuthProviderResponseError as er:
            self.redirect_to('add_account')
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)

    @webapp2.cached_property
    def session(self):
        return self.session_store.get_session()


    def _on_signin(self, data, auth_info, provider, extra=None):
        '''
            Callback cuando el usuario autoriza su cuenta con la app.
        '''

        user = self.user

        twitter_user = twitter.User(data)

        token = OAuthToken(key=auth_info['oauth_token'], secret=auth_info['oauth_token_secret'])

        # // As Transaction

        twitter_account = TwitterAccount(parent=user.key,
                                         id=twitter_user.screen_name,
                                         screen_name=twitter_user.screen_name,
                                         user_id=twitter_user.id,
                                         name=twitter_user.name,
                                         image_url=twitter_user.profile_image_url,
                                         token=token,
                                         language =twitter_user.lang,
                                         profile_banner_url=twitter_user.profile_banner_url
                                         )    
        if TwitterAccount.query(TwitterAccount.screen_name == twitter_account.screen_name).get():            
            params = {}
            params['screen_name'] = twitter_account.screen_name
            self.render('go_dashboard.html', params)
        else:
            if user.add_account(twitter_account.key):
                twitter_account.put()
                user.put()

                historic_sample = FollowersHistoric.query(ancestor=twitter_account.key).get()

                if not historic_sample:

                    day = datetime.now() - timedelta(days=1)
        
                    followers = FollowersHistoric(count=twitter_user.followers_count,
                                              parent=twitter_account.key,
                                              day=day)
                    followers.put()

                eticket = ExtensionTicket.query(ancestor = user.key).filter(
                                            ExtensionTicket.origin == 'new_user',
                                            ExtensionTicket.consumed == False).get()

                if (eticket):
                    eticket.apply_extension(twitter_account)
                else:
                    self.redirect_to('extend_form', screen_name=twitter_account.screen_name)
                    return
            self.redirect_to('dashboard', screen_name=twitter_account.screen_name)
        
    def _callback_uri_for(self, provider):
        return self.uri_for('auth_callback', provider=provider, _full=True)

    def _get_consumer_info_for(self, provider):
        return (TWITTERAPP_CONSUMER_DATA['KEY'], TWITTERAPP_CONSUMER_DATA['SECRET'])



