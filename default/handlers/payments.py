#encoding:utf-8
'''
Created on 27/02/2016

@author: capi
'''

from datetime import datetime
import logging

from google.appengine.api import mail
from google.appengine.ext import deferred
from google.appengine.ext import ndb
from webapp2 import uri_for
import webapp2
from webapp2_extras import jinja2

from configs import PRICES
from configs import SENDER_EMAIL
from handlers.abstracts import AccountManagerHandler
import mercadopago
from models import PaymentOrder, ExtensionTicket
from mail_data import mail_templates
from mail_utils import send_mail

import os
import paypalrestsdk
from urllib import urlencode
import urllib
import httplib2
import urlparse

def get_mercadopago_client():

    mp_client = os.environ['MP_CLIENT_ID']
    mp_secret = os.environ['MP_CLIENT_SECRET']

    mp = mercadopago.MP(mp_client, mp_secret) 
    #mp = mercadopago.MP('7646075133248733', 'wJxY9g7SogKnFUNI92RjqotaWyqJSP6h') #Usuario de prueba user: TETE3371513, pass: qatest1785
    #mp.sandbox_mode(True)
    return mp

def get_paypal_client():
    
    pp_client = os.environ['PP_CLIENT_ID']
    pp_secret = os.environ['PP_CLIENT_SECRET']
    pp_mode = 'sandbox' if os.environ['SANDBOX_MODE'] == 'True' else 'live'
    

    print "pp_mode: " + pp_mode

    pp = paypalrestsdk.configure({
        'mode': pp_mode,
        'client_id': pp_client,
        'client_secret': pp_secret
    })

    return pp

class PaymentNotificationHandler(webapp2.RequestHandler):
    
    def _mail_notification(self, payment_order):
        
        owner = payment_order.payer.get() # TODO: Quizás deberia venir desde el owner del extension ticket
        ticket = payment_order.get_ticket()
        account = ticket.get_twitter_account()

        mail_content = mail_templates['payment_successful']
        mail_content['server_url'] = self.request.host_url
        mail_content['body'] = mail_content['body'] % \
        {'screen_name': account.screen_name}
        mail_content['greeting'] = mail_content['greeting'] % \
        {'name': account.name}
        mail_content['button_url'] = mail_content['button_url'] % \
        {'server':mail_content['server_url'], 'screen_name': account.screen_name}
        
        send_mail(jinja2.get_jinja2(app=self.app), owner.email, mail_content['title_mail'], mail_content)

        logging.info("Pago consolidado para usuario " + owner.email)

    def _process_payment(self, provider_response, processor):
        
        if processor == 'mercadopago':
            mp = get_mercadopago_client()
            processor_payment = mp.get_payment(provider_response)
        elif processor == 'paypal':
            processor_payment = provider_response                   

        payment_order = PaymentOrder.consolidate(processor_payment, processor)

        if payment_order:
            if payment_order.consolidated_at:                
                self._mail_notification(payment_order)

class MercadopagoPaymentHandler(AccountManagerHandler):

    def create_order(self, screen_name):
        """
            Crea una orden de pago de tipo mercadoPago y redirige al checkout de este.
        """

        # El monto puede no venir de un form sino definido a partir de los parámetros como la cantidad de meses.

        months_quantity = int(self.request.get('months_quantity'))
        mount = float(months_quantity*PRICES['VEF'])

        print mount


        payment_id = PaymentOrder.allocate_ids(1)[0]

        payment_order = PaymentOrder(id=payment_id, processor='mercadopago', mount=mount, payer=self.user.key, currency="VEF" )

        eticket = ExtensionTicket(origin='pay', owner=self.user.key, long=months_quantity*30,pay_order=payment_order.key, twitter_account=self.account_selected.key)

        preference = {
            'items':
            [   
              {
                'title': 'Socializr: Extensión de operación por 30 días sobre cuenta @' + screen_name,
                'quantity': months_quantity,
                'currency_id': 'VEF',
                'unit_price': PRICES['VEF']
               }
             ],
            'back_urls': {
                'success': self.request.host + uri_for('mp_payment_success', screen_name=screen_name)
            },
            'payer':{
                'email': self.user.email
            },
            'external_reference': payment_order.key.id()
        }

        mp = get_mercadopago_client()

        preference_result = mp.create_preference(preference)

        payment_order.mp_preference_id = preference_result['response']['id']
        ndb.put_multi([payment_order, eticket])

        #url = preference_result["response"]["sandbox_init_point"]
        url = preference_result["response"]["init_point"]

        self.redirect(str(url))

    def payment_success(self, screen_name):

        #payment_id = int(self.request.get('external_reference'))

        approved = self.request.get('collection_status') == 'approved'

        if approved:
            self.render('payment_success.html',{
                                               'url_dashboard': self.uri_for('dashboard', screen_name=screen_name)})
        else:
            self.redirect_to('dashboard', screen_name=screen_name)

    def get(self, screen_name):

        vars = {
                'create_order_url': uri_for('create_order', screen_name=screen_name)
                }

        self.render('do_extension.html', vars)

class MercadopagoNotificationHandler(PaymentNotificationHandler):

    def post(self, **kwargs):

        print self.request

        if self.request.get('topic') == 'payment':
            #deferred.defer(self._process_payment, self.request.get('id')) # TODO like defer
            self._process_payment(self.request.get('id'), 'mercadopago')
            logging.info( "Se procesó el pago")

        logging.info( "Llega notificación desde el procesador de pagos")

        self.response.write("Llega notificación del procesador de pagos.")

class PaypalPaymentHandler(AccountManagerHandler):
    
    def create_order(self, screen_name):
                        
        months_quantity = int(self.request.get('months_quantity'))
        mount = float(months_quantity*PRICES['USD'])
        payment_id = PaymentOrder.allocate_ids(1)[0]
        payment_order = PaymentOrder(id= payment_id, processor='paypal', mount=mount, payer=self.user.key, currency='USD')        
        eticket = ExtensionTicket(origin='pay', owner=self.user.key, long=months_quantity*30, pay_order=payment_order.key, twitter_account=self.account_selected.key)

        pp = get_paypal_client()

        payment = paypalrestsdk.Payment({
            'intent': 'sale',
            'payer':{
                'payment_method':'paypal'
            },
            'redirect_urls':{
                'return_url': self.request.host_url + uri_for('pp_payment_success', screen_name=screen_name),
                'cancel_url': self.request.host_url + '/payment/cancel'
            },
            'transactions':[{
                "item_list": {
                    "items": [{
                        "name": screen_name,
                        "sku": screen_name,
                        "price": PRICES['USD'],
                        "currency": "USD",
                        "quantity": 1}]
                },
                'amount': {
                    'total': PRICES['USD'],
                    'currency': 'USD'
                },
                'description': u'Socializr: Extensión de operación por 30 días sobre cuenta @' + screen_name,
                'custom': payment_order.key.id()
            }]
        })

        if payment.create():
            pp_payment_id = payment['id']          

            payment_order.pp_payment_id = pp_payment_id
            ndb.put_multi([payment_order, eticket])

            for link in payment.links:
                if link.method == 'REDIRECT':
                    url = link.href
                    self.redirect(str(url))
        else: 
            logging.error(payment.error)
            self.response.write(payment.error)

    def payment_success(self, screen_name):
        
        payment_id = self.request.get('paymentId')
        payer_id = self.request.get('PayerID')

        get_paypal_client()

        payment = paypalrestsdk.Payment.find(payment_id)

        if payment.execute({'payer_id': payer_id}):
            self.render('payment_success.html',{'url_dashboard': self.uri_for('dashboard', screen_name=screen_name)})
        else:
            self.redirect_to('dashboard', screen_name=screen_name)

    def get(self, screen_name):
    
        vars = {
                'create_order_url': uri_for('create_order', screen_name=screen_name)
                }

        self.render('do_extension.html', vars)

class PaypalNotificationHandler(PaymentNotificationHandler):

    def _verify_ipn(self, data):
        
        pp_sandbox_mode = os.environ['SANDBOX_MODE'] == 'True'
        
        VERIFY_URL_PROD = 'https://ipnpb.paypal.com/cgi-bin/webscr'
        VERIFY_URL_TEST = 'https://www.sandbox.paypal.com/cgi-bin/webscr'

        # Switch as appropriate
        VERIFY_URL = VERIFY_URL_TEST if pp_sandbox_mode else VERIFY_URL_PROD

        if data:            
            body = data + "&cmd=_notify-validate"
            
            # Post back to PayPal for validation
            headers = {'content-type': 'application/x-www-form-urlencoded', 'host': 'www.paypal.com'}
            http = httplib2.Http()

            url = VERIFY_URL  
            response, content = http.request(url, 'POST', headers=headers, body=body)

        return content

    def post(self, **kwargs):
        
        data_body = self.request.body
        data = dict(urlparse.parse_qsl(data_body))

        logging.debug(self.request.body)

        txn_id = str(data.get('txn_id'))
        txn_id_status = PaymentOrder.query(PaymentOrder.pp_transaction_id == txn_id).get()

        if txn_id_status == None:
            content = self._verify_ipn(data_body)
            if content == 'VERIFIED':            
                self._process_payment(data, 'paypal')
            else:
                logging.warn("No verificado. Se responde con status 500")
                raise Exception()
