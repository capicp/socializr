# -*- coding: utf-8 -*-
'''
Created on 24/12/2015

@author: capi
'''
from datetime import datetime, date
import logging

from google.appengine.api import users
from google.appengine.api.taskqueue import Task
from google.appengine.ext import ndb
from webapp2 import uri_for
from webapp2_extras.auth import InvalidAuthIdError, InvalidPasswordError
from webapp2_extras import jinja2

from abstracts import BaseRequestHandler, AccountManagerHandler
import configs
from models import User, TwitterAccount, Audience, ExtensionTicket
from utils import user_required, admin_required
from mail_utils import send_mail
from configs import PRICES, LIMIT_AUDIENCE
from utils import to_vef_format

#from tasks import EngageAccountTask
class DashboardPage(AccountManagerHandler):

    @user_required
    def get(self, screen_name):
        params = {}
        params['subscription_remaining_days'] = 0
        params['alert_components'] = []

        if not self.account_selected:
            
            self.redirect_to('add_account')
            return

        #send_mail(self.jinja2, "cpinelly@gmail.com", "Prueba de sendgrid", {})

        time_to_due = None
        if self.get_selected_account().due_date is not None:
            now = date.today()
            time_to_due = self.get_selected_account().due_date - now
            params['subscription_remaining_days'] = time_to_due.days

        if (time_to_due is None or time_to_due.days <= 0):
            params['alert_components'].append({
                    'url': uri_for('extend_form', screen_name=screen_name),
                    'message': 'La suscripción de la cuenta necesita ser renovada para que Socializr continue operando sobre la misma.'.decode('utf-8'),
                    'text_button': 'Extender cuenta'
            })  

        if self.account_selected.has_active_audiences() < 1:
            params['alert_components'].append({
                'url': uri_for('audience_editor', screen_name=screen_name),
                'message': 'Para comenzar a utilizar Socializr es necesario definir algunos keywords, los cuales especificarán el público al que desea apuntar.'.decode('utf-8'),
                'text_button': 'Configurar Keywords'
            })
                
        self.render('dashboard.html', params)

    @user_required
    def post(self, screen_name):

        account_key = self.request.get('account_key')

        #EngageAccountTask(account_key=account_key).add()

        params = {}
        params['account_key'] = account_key

        Task(url='/tasks/engageAccount', method='POST', params=params).add(queue_name='engagement')


        self.redirect_to('audiences', screen_name=screen_name)

class AudienceManagerHandler(AccountManagerHandler):

    # --------- Handlers

    @user_required
    def archive(self, audience_key, screen_name):

        audience = ndb.Key(urlsafe=audience_key).get()

        audience.archived = True
        audience.put()

        self.redirect_to('audiences', screen_name=screen_name)

    @user_required
    def delete(self, audience_key, screen_name):

        audience = ndb.Key(urlsafe=audience_key).get()
        audience.delete()

        self.redirect_to('audiences', screen_name=screen_name)

    @user_required
    def list(self, screen_name, params={}):

        self.account_selected.active_audiences = self.account_selected.get_active_audiences()

        ordered_audiences = sorted(self.account_selected.active_audiences, key= lambda audience: audience.get_successful_rate() )
        self.account_selected.active_audiences = ordered_audiences

        for audience in self.account_selected.active_audiences:

            audience.url_archive = uri_for('audience_archive', screen_name=screen_name, audience_key=audience.key.urlsafe() )
            audience.url_delete = uri_for('audience_delete', screen_name=screen_name, audience_key=audience.key.urlsafe() )

        params['url_audience_form'] = uri_for('audience_editor', screen_name=screen_name)

        self.render('audiences_list.html', params)

    @user_required
    def audience_editor(self, screen_name):

        self.account_selected.active_audiences = self.account_selected.get_active_audiences()

        params = {
                  'audience_save_url': uri_for('audience_save', screen_name=screen_name)
                  }

        self.render('keywords_editor.html', params)

    @user_required
    def post(self, screen_name=None):
        
        query = self.request.get('search_query')
        account_key_str = self.request.get('account_key')

        account_key = ndb.Key(urlsafe=account_key_str)

        params = {}

        if Audience.query(ancestor=account_key).count() < LIMIT_AUDIENCE:
            if not Audience.exist_query_for_account(query, account_key):
                audience = Audience(search_query=query, parent=account_key)
                audience.put()
                
                params['account_key'] = account_key_str
                params['only_news'] = True

                #eta = datetime.now() + configs.TIME_FOR_NEW_AUDIENCES

                #Task(url='/tasks/engageAccount', method='POST', params=params, eta=eta).add(queue_name='engagement')

            else:
                logging.debug('Audiencia ya registrada para esta cuenta')
            
            self.redirect_to('audiences', screen_name=screen_name)
            
        else:
            params['alert_components'] = []

            params['alert_components'].append({
                    'message': 'Puede agregar un máximo de 15 keywords por cuenta.'.decode('utf-8')
            })

            self.list(screen_name, params)

        

class PaymentsHandler(AccountManagerHandler):

    def list(self, screen_name=None):

        params = {}

        tickets = ExtensionTicket.query(ExtensionTicket.twitter_account == self.account_selected.key, 
        ExtensionTicket.pay_order != None).fetch()
        
        params['tickets'] = sorted(tickets, key= lambda tickets: tickets.consumed  ) 

        params['extend_form_url'] = uri_for("extend_form", screen_name=screen_name)
        self.render("account_payments_list.html", params)



class UserAuthHandler(BaseRequestHandler):


    @user_required
    def add_account(self):
        user_model = self.user
        
        if (self.request.url == (self.request.host_url + '/account/add')) or (user_model and len(user_model.account_keys) < 1):
            self.render('add_account.html', {'user': user_model, 'logout_url': self.uri_for('logout')})
        else:
            if user_model and len(user_model.account_keys) > 0:
                account = user_model.account_keys[0].get()
                self.redirect_to('dashboard', screen_name=account.screen_name)
    
    def post(self):
        continue_page = self.request.GET.get('continue')
        screen_name = self.request.GET.get('screen_name')

        google_user = self.google_user

        if google_user:
            email = google_user.email()
            password = google_user.user_id()
            google_id = google_user.user_id()

        else:
            email = self.request.get('email')
            password = self.request.get('password')
            google_id = None

            try:
                u = self.auth.get_user_by_password(email, password, remember=True)

                if continue_page:
                    url_page = '%s/%s/%s/%s' % (self.request.host_url, 'editor', screen_name, continue_page) 
                    self.redirect(str(url_page))
                else:
                    self.redirect(self.uri_for('add_account'))
                
            except (InvalidAuthIdError, InvalidPasswordError) as e:

                params = {
                    'user_email': email,
                    'error_message': u'Correo o contraseña incorrecta.',
                    'signup_url': uri_for('signup')
                }

                self.render('login_form.html', params)
    


    @user_required
    def logout(self):
        self.auth.unset_session()
        self.redirect_to('login')

    def get(self):

        if (self.user_info):
            self.redirect_to('add_account')
            return
        
        params = {
            'signup_url': self.uri_for('signup')
        }

        continue_page = self.request.GET.get('continue')
        screen_name = self.request.GET.get('screen_name')

        if continue_page:
            params['url_params'] = '?continue=%s&screen_name=%s' % (continue_page, screen_name)

        self.render('login_form.html', params)

        
class UserSignUp(BaseRequestHandler):
    
    def get_base_params(self):
        return {
            'login_url': self.uri_for('login')
        }
    
    def get(self):
        
        if (self.user_info):
            self.redirect_to('add_account')
            return
        
        self.render('signup_form.html', self.get_base_params() )

    def post(self):
        
        email = self.request.get('email')
        password = self.request.get('password')

        params = self.get_base_params()
        params['user_email'] = email

        unique_properties = ['email']

        user_data = self.user_model.create_user(email,
          unique_properties,
          email=email, password_raw=password,
           verified=True)

        if not user_data[0]: # the email is in use
            
            params['error_message'] = u'Dirección de correo en uso.'
            
            self.render('signup_form.html', params)

            # TODO: No es lo ideal
            return
        else: # new user
            user = user_data[1]
            self.auth.set_session(self.auth.store.user_to_dict(user), remember=True)

            trial_extension = ExtensionTicket(origin='new_user', long=15, parent=user.key)
            trial_extension.put()

            self.redirect(uri_for('add_account'))

        


class AccountHandler(AccountManagerHandler):

    @user_required
    def extend_form(self, screen_name):

        params = {
                  'url_create_order': uri_for('pp_create_order', screen_name=screen_name),
                  'url_dashboard': uri_for('dashboard', screen_name=screen_name),
                  'price': '' + to_vef_format( PRICES['USD'] )
        }

        self.render('extend_form.html', params)

class LandingHandler(BaseRequestHandler):

    def get(self):
        user = self.user
        auth = self.auth
                
        if auth.get_user_by_session():
            twitter_user = TwitterAccount.query(ancestor=user.key).get()
            self.redirect_to('dashboard', screen_name=twitter_user.screen_name)
        else:

            params = {
                    'price': '' + to_vef_format( PRICES['USD'] ),
                    'login_url': uri_for('login'),
                    'signup_url': uri_for('signup')
            }

            self.render('landing.html', params)

class LoginAdminHandler(BaseRequestHandler):

    @admin_required
    def get(self):
        self.redirect(uri_for('admin_dashboard'))    

class DashboardAdminHandler(BaseRequestHandler):
    
    @admin_required
    def get(self):
        
        accounts = TwitterAccount.get_active_accounts()

        params = {
            'logout_url': users.create_logout_url('/'),
            'accounts': accounts,
            'active_accounts':accounts.count(),
            'registered_accounts': TwitterAccount.query().count(),
            'server_host': self.request.host_url
        }

        self.render('admin_dashboard.html', params)