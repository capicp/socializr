#encoding: utf-8
'''
Created on 06/02/2016

@author: capi
'''

from datetime import timedelta
import os
from google.appengine.api.app_identity import get_application_id
appname = get_application_id()


# Tiempo que se esperará para ejecutar una tarea de engagement (sólo para keywords nuevas) desde que se agrega una nueva keyword.
TIME_FOR_NEW_AUDIENCES = timedelta(minutes=80)

LIMIT_AUDIENCE = 15

# Dirección de email usada para enviar correos a usuarios
SENDER_EMAIL = 'noreply@' + appname + '.appspotmail.com'

PRICES = {
          'VEF': 6800,
          'USD': 9
}

if 'Development' in os.environ['SERVER_SOFTWARE']:

    # We are in development

    TIME_FOR_NEW_AUDIENCES = timedelta(seconds=5)