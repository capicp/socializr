'''
Created on 24/12/2015

@author: capi
'''
from webapp2 import Route


ROUTES = [
        Route('/', handler='handlers.pages.LandingHandler', name='landing'),
        Route('/start', handler='handlers.pages.UserAuthHandler:add_account', name='add_account'),
        Route('/editor/<screen_name>/dashboard', handler="handlers.pages.DashboardPage", name='dashboard'),

        Route('/login', handler="handlers.pages.UserAuthHandler", name="login"),
        Route('/signup', handler="handlers.pages.UserSignUp", name="signup"),
        Route('/logout', handler="handlers.pages.UserAuthHandler:logout", name="logout"),

        Route('/editor/<screen_name>/extend', handler="handlers.pages.AccountHandler:extend_form", name="extend_form"  ),

        Route('/editor/<screen_name>/payments/mercadopago/form', handler="handlers.payments.MercadopagoPaymentHandler",),
        Route('/editor/<screen_name>/payments/mercadopago/create', handler="handlers.payments.MercadopagoPaymentHandler:create_order", name="mp_create_order"),
        Route('/editor/<screen_name>/payments/mercadopago/success', handler="handlers.payments.MercadopagoPaymentHandler:payment_success", name="mp_payment_success"),

        # IMPORTANTE: Si se cambia esta direccion, recordar que debe cambiarse en la configuracion de mercadopago: https://www.mercadopago.com.ve/ipn-notifications
        Route('/hooks/payments/mercadopago/notification', handler="handlers.payments.MercadopagoNotificationHandler"),

        
        Route('/editor/<screen_name>/payments/paypal/form', handler='handlers.payments.PaypalPaymentHandler'),
        Route('/editor/<screen_name>/payments/paypal/create', handler="handlers.payments.PaypalPaymentHandler:create_order", name="pp_create_order"),
        Route('/editor/<screen_name>/payments/paypal/success', handler="handlers.payments.PaypalPaymentHandler:payment_success", name="pp_payment_success"),       
        
        Route('/hooks/payments/paypal/notification', handler='handlers.payments.PaypalNotificationHandler', name="paypal_notification"),


        Route('/checkout', handler="handlers.pages.CheckoutHandler", name="checkout"),

        Route('/editor/<screen_name>/keywords', handler='handlers.pages.AudienceManagerHandler:list', name="audiences"),
        Route('/editor/<screen_name>/keyword/save', handler='handlers.pages.AudienceManagerHandler', name="audience_save"),
        Route('/editor/<screen_name>/keywords/editor', handler='handlers.pages.AudienceManagerHandler:audience_editor', name="audience_editor"),
        Route('/editor/<screen_name>/keyword/archive/<audience_key>', handler='handlers.pages.AudienceManagerHandler:archive', name="audience_archive"),
        Route('/editor/<screen_name>/keyword/delete/<audience_key>', handler='handlers.pages.AudienceManagerHandler:delete', name="audience_delete"),

        Route('/editor/<screen_name>/payments', handler='handlers.pages.PaymentsHandler:list', name="payments"),


        Route('/editor/<screen_name>/engage', handler='handlers.pages.DashboardPage', name="account_engage"),

        Route('/account/add', handler='handlers.pages.UserAuthHandler:add_account', name='add_other_account'),
        
        Route('/auth/<provider>', handler='handlers.auth.TwitterAuthHandler:_simple_auth', name='auth_login'),        
        Route('/auth/<provider>/callback', handler='handlers.auth.TwitterAuthHandler:_auth_callback', name='auth_callback'),

        Route('/tasks/sendMailExpiredAccount', handler='handlers.tasks.SendMailExpiredAccountHandler', name="mail_expired_account"),

        Route('/test/account/<screen_name>/expired', handler='handlers.tests.ExpiredAccountHandler', name="expired_account_test"),

        Route('/admin', handler='handlers.pages.LoginAdminHandler', name='login_admin'),
        Route('/admin/dashboard', handler='handlers.pages.DashboardAdminHandler', name='admin_dashboard'),

    ]
