'''
Created on 12/02/2016

@author: capi
'''

from protorpc import messages


class FollowersHistoric(messages.Message):

    count = messages.IntegerField(1)
    day = messages.StringField(2)

class FollowersGrowth(messages.Message):

    historic = messages.MessageField(FollowersHistoric, 1, repeated=True)

class TwitterUser(messages.Message):

    id = messages.IntegerField(1)
    name = messages.StringField(2)
    screen_name = messages.StringField(3)
    description = messages.StringField(4)
    lang = messages.StringField(5)
    profile_image_url = messages.StringField(6)
    followers_count = messages.IntegerField(7)
    user_url = messages.StringField(8)

class TwitterUserCollection(messages.Message):

    users = messages.MessageField(TwitterUser, 1, repeated=True)





