'''
Created on 23/01/2016

@author: capi
'''
from twitterapi.objects import ApiError
package = 'Api'

from datetime import datetime, timedelta

import endpoints
from google.appengine.ext import ndb
from protorpc import message_types, messages
from protorpc import remote

import api.custom_messages as cm
from miner_core import TargetSeeker
import models
import secrets

import pdb


@endpoints.api(name="miner", version="v1")
class MinerApi(remote.Service):

    ACCOUNT_ID_AND_LIMIT = endpoints.ResourceContainer(message_types.VoidMessage,
                                             screen_name=messages.StringField(1),
                                             limit = messages.IntegerField(2)
                                             )

    ACCOUNT_ID = endpoints.ResourceContainer(message_types.VoidMessage,
                                             screen_name=messages.StringField(1)
                                             )

    KEYWORD_TEST = endpoints.ResourceContainer(message_types.VoidMessage,
                                               screen_name=messages.StringField(1),
                                               keyword=messages.StringField(2),
                                               count=messages.IntegerField(3)
                                               )

    @endpoints.method(ACCOUNT_ID_AND_LIMIT, cm.FollowersGrowth,
                      path="/followers/historic/{screen_name}", http_method="GET",
                      name="followers.historic")
    def followers_historic(self, request):

        response = cm.FollowersGrowth()

        account = models.TwitterAccount.get_by_screenname(request.screen_name).get()

        historics = models.FollowersHistoric.query(ancestor=account.key).filter(models.FollowersHistoric.day >= (datetime.now() - timedelta(days=request.limit))).order(models.FollowersHistoric.day)

        collection = []

        for historic in historics:

            message = cm.FollowersHistoric(count=historic.count, day=historic.day.isoformat())
            collection.append(message)

        response.historic = collection

        return response

    @endpoints.method(KEYWORD_TEST, cm.TwitterUserCollection,
                      path="/audience/sample", http_method="GET",
                      name="audience.sample")
    def get_audience_sample(self, request):

        account = models.TwitterAccount.get_by_screenname(request.screen_name).get()

        response = cm.TwitterUserCollection()

        token_data = {
                      'KEY': account.token.key,
                      'SECRET': account.token.secret
                      }

        seeker = TargetSeeker(request.keyword, secrets.TWITTERAPP_CONSUMER_DATA, token_data)

        try:
            seeker.search(None, request.count)
        except ApiError:
            return response

        users = seeker.get_target_users()

        for user in users:

            user_message = cm.TwitterUser(id=user.id,
                                          name=user.name,
                                          screen_name=user.screen_name,
                                          description=user.description,
                                          lang=user.lang,
                                          profile_image_url=user.profile_image_url,
                                          followers_count=user.followers_count,
                                          user_url=user.get_url()
                                            )
            response.users.append(user_message)

        return response




    # For development

    @endpoints.method (ACCOUNT_ID, message_types.VoidMessage,
                       path="/dev/followers_historic/make/{screen_name}", http_method="GET",
                       name="dev.followers_historic.make")
    def make_followers_historic(self, request):

        from datetime import datetime
        from datetime import timedelta

        import random

        now = datetime.now()

        followers_historics = []

        r = range(0,45)
        r.reverse()

        for i in r:

            count = 300 - i*5 +  random.randint(5, 15)
            day = now - timedelta(days=i)
            m = models.FollowersHistoric(count=count, day=day, parent=ndb.Key(models.TwitterAccount, request.screen_name) )
            m.put()

        return message_types.VoidMessage()

app = endpoints.api_server([MinerApi])




