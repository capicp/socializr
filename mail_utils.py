#!/usr/bin/env python
# -*- coding: utf-8 -*-

from google.appengine.api import mail
from configs import SENDER_EMAIL

import os
import logging

import sendgrid
from sendgrid.helpers.mail import *
  
def send_mail(jinja, mail_to, mail_subject, mail_data):
    logging.info("Email data: %s" % (mail_data))

    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))

    from_email = Email("hola@socializr.co")
    to_email = Email(mail_to)

    content_text = jinja.render_template('email_generic.html', **mail_data)

    content = Content("text/html", content_text)

    mail = Mail(from_email, mail_subject, to_email, content)

    response = sg.client.mail.send.post(request_body=mail.get())

    logging.info("Sendgrid response %s: %s " % (response.status_code, response.body))
    

def get_public_host():
    public_host = os.environ['PUBLIC_HOST']
    if public_host is not None and public_host is not '':
        return public_host
    else:
        return None