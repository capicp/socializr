
var $loader = $('#loader');
var $editorWrapper = $('#editor_wrapper');

$editorWrapper.hide();

function onEndpointsLoaded(){

  var $viewAudienceButton = $('#view_audience');
  var $keywordInput = $('#keyword_input');
  var $keywordExplainContainer = $('#keyword_explain');
  var $keywordSampleContainer = $('#audience_sample_message');
  var $audienceListContainer = $('#audience_list');
  var $audienceSampleMessage = $('#audience_sample_message');

  var AUDIENCE_SAMPLE_COUNT = 20;

  var user_template = $('#user_template').html();

  var account_id = $('#account_id').html()

  $keywordSampleContainer.hide();


  function initUIEvents(){

    $viewAudienceButton.on('click', function(e){
      e.preventDefault();

      $audienceSampleMessage.html("Obteniendo audiencia objetivo...");
      $audienceListContainer.html("");

      var keyword = $keywordInput.val();

      $keywordExplainContainer.hide();
      $keywordSampleContainer.show();

      gapi.client.miner.audience.sample({keyword: keyword, screen_name: account_id, count: AUDIENCE_SAMPLE_COUNT }).execute(function(response){

        $audienceSampleMessage.html("");

        if (!response.users || response.users.length == 0){
          $keywordSampleContainer.html("No se encontró audiencia para este keyword, se recomienda probar con otra posibilidad.")
        }else{
          var audience_sample_list = '';

          for (i = 0; i < response.users.length; i++ ){

              audience_sample_list = audience_sample_list + Mustache.render(user_template, response.users[i]);
          }

          $audienceListContainer.html(audience_sample_list);

          if (response.users.length < AUDIENCE_SAMPLE_COUNT){
            $audienceSampleMessage.html("Este keyword no apunta a suficiente audiencia, se recomienda probar con otra posibilidad.")
          }
        }        

      });
    })
  }

  initUIEvents();

  $editorWrapper.show();
  $loader.hide();
};
