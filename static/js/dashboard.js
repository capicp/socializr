
var account_id = $("#account_id").html();

var $metric_new_followers = $('#metric_new_followers');
var $metric_average_followers = $('#metric_average_followers');
var $metric_forecast = $('#metric_forecast');
var $days_analized = $('#days_analyzed');

function activateLoader(element_id, activate){
  if (activate){
    $('#' + element_id).show();
  }else{
    $('#' + element_id).hide();
  }  
}

function onEndpointsLoaded(){
  
    gapi.client.miner.followers.historic({screen_name: account_id, limit: 31}).execute(function(followers_growht){
      drawFollowersChart(followers_growht.historic)})
}

function drawFollowersChart(followers_historics){

  google.charts.load('current', {'packages':['corechart', 'bar']});
  google.charts.setOnLoadCallback(drawChart);

  var less_followers = _.min(followers_historics, function(h){return h.count})
  var actual_followers = followers_historics[followers_historics.length-1].count*1;

  function drawChart() {

    data = [
      ['Día', 'Seguidores']
    ]

    for (i = 0; i < followers_historics.length; i++){

      console.log('Se itera')

      var row = [moment(followers_historics[i].day).format("D MMM"),
                 followers_historics[i].count*1]


      data.push(row)
    }

    console.log(data)

    var data = google.visualization.arrayToDataTable(data);

    var options = {
      chartArea: {
        top: 24,
        left: 60,
        right: 20
      },
      colors: ['#0bb6d6'],
      pointSize: 4,
      legend: {position: 'none'},
      axisTitlesPosition: 'none',
      hAxis: {
        titleTextStyle: {
          fontSize: 5
        }
      },
      vAxis: {minValue: less_followers.count} // TODO: Valor mínimo de seguidores
    };

    var chart = new google.visualization.AreaChart(document.getElementById('followers_chart'));
    activateLoader('loader-followers_chart', false);
    $('#followers_chart').show();
    chart.draw(data, options);

    new_followers = [['Días', 'Seguidores']]

    var sum = 0;


    for (i=1; i < followers_historics.length; i++){

      if ( moment(followers_historics[i].day).date() - moment(followers_historics[i-1].day).date() == 1 ){
    	  var point = [moment(followers_historics[i].day).format("D MMM"), (followers_historics[i].count*1)-(followers_historics[i-1].count*1)]
          new_followers.push(point);
      }


      //regression_points.push([i,followers_historics[i].count*1]);

      if (point){
        sum = sum + point[1];
      }
      
    }

    var days_analyzed = moment(followers_historics[followers_historics.length-1].day)
    						.diff(followers_historics[0].day, 'days') //Diferencia en dias entre los dos puntos evaluados

/*
    var n = regression_points.length - 1;

    for ( i = n ; i < (n+30); i++){
      regression_points.push([i,null]);
    }

    console.log(regression_points);
*/

    //result = regression('polynomial', regression_points);

    //console.log(result);

    var new_followers_data = google.visualization.arrayToDataTable(new_followers);

    options['colors'] = "#0bb6d6"

    var chart = new google.charts.Bar(document.getElementById('new_followers_chart'));

    activateLoader('loader-new_followers_chart', false);
    $('#new_followers_chart').show();
    chart.draw(new_followers_data, options);

	if (days_analyzed){
		var average = Math.round(sum / days_analyzed);

	    $metric_average_followers.html(average);
	    $metric_new_followers.html(sum);
	    $days_analized.html(days_analyzed);

	    var forecast = (30*average) + actual_followers;
	    $metric_forecast.html(forecast);
	}


  }
}
