
/** google global namespace for Google projects. */
var google = google || {};

/** appengine namespace for Google Developer Relations projects. */
google.appengine = google.appengine || {};

/** samples namespace for App Engine sample code. */
google.appengine.miner= google.appengine.miner || {};

google.appengine.miner.init = function(apiRoot) {
  // Loads the OAuth and helloworld APIs asynchronously, and triggers login
  // when they have completed.
  var apisToLoad;
  var callback = function() {
    if (--apisToLoad == 0) {
        onEndpointsLoaded()
    }
  }

  apisToLoad = 1; // must match number of calls to gapi.client.load()
  gapi.client.load('miner', 'v1', callback, apiRoot);
};

function init(){

  var host = window.location.host;
  var endpointsHost = '//' + host;

  if (host.indexOf('localhost') == -1){
    endpointsHost = document.getElementById("endpoints_host").textContent;
  }
  
  google.appengine.miner.init(endpointsHost + '/_ah/api');
}
