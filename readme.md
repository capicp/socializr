
## Comandos de desarrollo

Para correr ambas aplicaciones
dev_appserver.py default/app.yaml miner/miner.yaml

## Comandos de GAE

#### Actualizar proyecto en producción

gcloud app deploy app.yaml

#### Actualizar cron índices

gcloud app deploy index.yaml

#### Actualizar cron jobs

gcloud app deploy cron.yaml

#### Actualizar colas

gcloud app deploy queue.yaml

## Para pruebas en mercadopago

### Usuarios de mercadopago

- Comprador: TETE2367893 / qatest7157
- Vendedor: TETE7481596 / qatest8514

### Tarjetas de pruebas

Revisar: https://www.mercadopago.com.ve/developers/en/solutions/payments/basic-checkout/test/test-payments/

### Para crear usuarios de prueba 

https://www.mercadopago.com.ve/developers/en/solutions/payments/basic-checkout/test/test-users/

Tienen una duración de 60 días. Al crear nuevos usuarios recordar cambiar los usuario en este readme y configurar la url de notificación en el usuario vendedor.

### Configuración de notificaciones en mercadopago

https://www.mercadopago.com.ve/ipn-notifications

#### En caso de que python se mantenga en escucha del puerto 8000-8080

netstat -lnp

## Installing dependencies

pip install -r requirements.txt -t ./external_libs

