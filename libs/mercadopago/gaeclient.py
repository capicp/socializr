

import httplib2
import json
import urllib


def utf8encode_params(params):

    if params:
        for key in params.keys():

            if isinstance(params[key], basestring):

                params[key] = params[key].encode('utf8')

        return params

def make_post_request(uri, data=None, params=None, content_type="application/json" ):

    body = ''
    url = "https://api.mercadopago.com"
    method = 'POST'

    #parameters = {}

    header = {'User-Agent': 'MercadoPago Python SDK v0.3.4', 'Content-type':content_type, 'Accept':"application/json"}

    if data:
        url_params = utf8encode_params(data)
        uri = uri + "?" + urllib.urlencode(data)

    if params:
        encoded_params = utf8encode_params(params)
        body = str(params)
        #parameters.update(encoded_params)

    # TODO: If development enviroment
    #logging.info('Request to Twitter API ' + url)

    #request = oauth.Request(method='POST', url=url, parameters=parameters, body=body)
    #if encoded_params:
    #    request.is_form_encoded = True

#    self._sign_request(request)

    final_url = url + uri

    print final_url
    print body

    header, content = httplib2.Http().request(final_url, method=method, body=body,
        headers=header)

    print content

    response = header

    response['response'] = json.loads(content)

    response.update(header)

    return response