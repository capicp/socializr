'''
Created on 17/2/2016

@author: capi
'''
import logging

from twitterapi.clients import TwitterRESTClient
from twitterapi.objects import ApiError


class TargetSeeker():

    client = None
    query = ''
    users_found = []
    tweets_found = []

    def __init__(self, query, consumer_data={}, token_data={}, client=None):

        if client:
            self.client = client
        else:
            self.client = TwitterRESTClient(consumer_data=consumer_data, token_data=token_data)

        self.query = query

        self.users_found = []
        self.tweets_found = []

    def user_detected(self, user):
        """
            Indica si un usuario ya fue encontrado
        """

        for uf in self.users_found:
            if uf.id == user.id:
                return True

        return False

    def search(self, users_exclude=None, count=80, **kwargs):

        '''
            Retorna target tweets para realizar interacciones. Devuelve tweets con usuarios diferentes, que no sean seguidos
            y en un orden del mas antiguo al mas reciente.
        '''

        last_tweet = None

        max_loops = 10

        while( count > 0):

            if len(self.users_found) > 0 or kwargs.get('max_id') is not None:
                kwargs['count'] = count + 1
            else:
                kwargs['count'] = count

            if kwargs['count'] > 100:
                kwargs['count'] = 100   # Maximo permitido por el API

            try:
                tweets_list_ordered = self.client.search_tweets(query=self.query, include_entities=False, **kwargs)
            except ApiError as err:
                raise err

            if not tweets_list_ordered.tweets or len(tweets_list_ordered.tweets) == 0:
                logging.debug("No se encontraron tweets para la audiencia %s " % (self.query))
                break

            last_tweet = tweets_list_ordered.tweets[-1]
            kwargs['max_id'] = last_tweet.id

            tweet_ids = []
            for t in tweets_list_ordered.tweets:
                tweet_ids.append(t.id)

            statuses = self.client.statuses_lookup(tweet_ids, include_entities=False) #TODO: En el caso de 0 tweets

            # Verificamos si son usuarios diferentes a los encontados
            for status in statuses.tweets:
                if not users_exclude or not (status.user.id in users_exclude):
                    if not self.user_detected(status.user) and not status.user.following:    
                        # TODO: Deberia verificarse si el usuario ya nos sigue, en este caso tampoco es un user target
                        self.users_found.append(status.user)
                        self.tweets_found.append(status)

                        count = count - 1

            max_loops = max_loops - 1

            # TODO: Quitar si no se ve mas este error
            if max_loops < 1:
                logging.debug('Posiblemente era un loop infinito')
                logging.info('Count: %d' % count)
                logging.info('Recaudados: %d' % len(self.users_found))
                break

        self.tweets_found = sorted(self.tweets_found, key= lambda tweet: tweet.id)

        return self.tweets_found, self.users_found

    def get_target_users(self):

        return self.users_found

    def get_target_tweets(self):

        return self.tweets_found







