#encoding: utf-8
'''
Created on 24/12/2015

@author: capi
'''

from datetime import date, timedelta, datetime
import logging
import time

from google.appengine.ext import ndb
from webapp2_extras import security
import webapp2_extras.appengine.auth.models


class User(webapp2_extras.appengine.auth.models.User):

    #name = ndb.StringProperty('n')
    google_id = ndb.StringProperty('gid')
    account_keys = ndb.KeyProperty('a',repeated=True) # Twitter Accounts
    email = ndb.StringProperty('e')

    _accounts = None

    def get_accounts(self):

        if not self._accounts:
            self._accounts = ndb.get_multi(self.account_keys)

        return self._accounts

    def add_account(self, account_key):

        for a in self.account_keys:

            if (a.id() == account_key.id()):
                return False

        self.account_keys.append(account_key)

        return True

    # Métodos sobre autenticación adicionales

    def set_password(self, raw_password):

        self.password = security.generate_password_hash(raw_password, length=12)

    @classmethod
    def get_by_auth_token(cls, user_id, token, subject='auth'):

        """Returns a user object based on a user ID and token.

        :param user_id:
            The user_id of the requesting user.
        :param token:
            The token string to be verified.
        :returns:
            A tuple ``(User, timestamp)``, with a user object and
            the token timestamp, or ``(None, None)`` if both were not found.
        """
        token_key = cls.token_model.get_key(user_id, subject, token)
        user_key = ndb.Key(cls, user_id)
        # Use get_multi() to save a RPC call.
        valid_token, user = ndb.get_multi([token_key, user_key])
        if valid_token and user:
            timestamp = int(time.mktime(valid_token.created.timetuple()))
            return user, timestamp

        return None, None



class OAuthToken(ndb.Model):

    key = ndb.StringProperty('k')
    secret = ndb.StringProperty('s')
    updated_at = ndb.DateTimeProperty('u', auto_now=True)

class TwitterAccount(ndb.Model):

    screen_name = ndb.StringProperty('s')
    user_id = ndb.IntegerProperty('id')
    image_url = ndb.StringProperty('i')
    name = ndb.StringProperty('n')
    token = ndb.StructuredProperty(OAuthToken, 't')
    added_at = ndb.DateTimeProperty('a', auto_now_add=True)
    active = ndb.BooleanProperty('c', default=True) # Deprecated ?
    language = ndb.StringProperty('l', default='es')
    profile_banner_url = ndb.StringProperty('b')
    due_date = ndb.DateProperty('d')

    def has_active_audiences(self):
        return Audience.query(Audience.archived == False, ancestor=self.key).count(1)

    def get_active_audiences(self):

        return Audience.query(Audience.archived == False, ancestor=self.key)

    def get_new_audiences(self):

        active_audiences = self.get_active_audiences()

        new_audiences = []

        for audience in active_audiences:

            if not audience.has_engage_session():
                new_audiences.append(audience)

        return new_audiences

    @classmethod
    def get_active_accounts(cls):
        return cls.query(TwitterAccount.due_date >= date.today())
    
    def get_user_mail(self):
        """
            Deprecated
        """

        return User.query(User.account_keys == self.key)

    def get_user(self):
        return self.key.parent().get()

    @classmethod
    def get_by_screenname(cls, screen_name):
        return cls.query(TwitterAccount.screen_name == screen_name)

class FollowersHistoric(ndb.Model):

    count = ndb.IntegerProperty('f', required=True)
    day = ndb.DateTimeProperty('d', auto_now_add=True)

class Audience(ndb.Model):

    search_query = ndb.StringProperty('q', indexed=True)
    created_at = ndb.DateTimeProperty('c', auto_now_add=True)
    n_interactions = ndb.IntegerProperty('r', default=0, indexed=False)
    n_success = ndb.IntegerProperty('s', default=0, indexed=False)
    archived = ndb.BooleanProperty('a', default=False)

    def get_successful_rate(self):

        if self.n_interactions and self.n_success:
            return round( (self.n_success*100.0) / self.n_interactions, 1)
        else:
            if self.n_interactions:
                return 0
            else:
                return None

    def get_account_key(self):

        return self.key.parent()

    def delete(self):

        sessions = EngagementSession.query(EngagementSession.audience_key == self.key)

        for s in sessions:
            s.key.delete()

        self.key.delete()

    def has_engage_session(self):

        sample_session = EngagementSession.query(EngagementSession.audience_key == self.key).get()

        return sample_session is not None


    @classmethod
    def exist_query_for_account(cls, query, account_key):

        return cls.query(Audience.search_query == query, ancestor=account_key).get() is not None

    @ndb.transactional
    def add_engage_session(self, engage_session):

        n_actions = len(engage_session.actions)

        #self.n_interactions = self.n_interactions + n_actions
        engage_session.audience_key = self.key

        engage_session.put()
        self.put()

        return engage_session.key


class EngageAction(ndb.Model):

    TYPES = {
             'like': 'L',
             'follow': 'W'
             }

    type = ndb.StringProperty('t', choices=TYPES.values())
    user_target_id = ndb.IntegerProperty('u')
    status_target_id = ndb.IntegerProperty('s')
    cleaned = ndb.BooleanProperty('c') # In case of partials cleans for exceptions. TODO: Implement with rate error
    success = ndb.BooleanProperty('x', default=False)

    def set_as_like(self):
        self.type = self.TYPES['like']

    def set_as_follow(self):
        self.type = self.TYPES['follow']

    def is_follow(self):
        return self.type == self.TYPES['follow']

    def is_like(self):
        return self.type == self.TYPES['like']

class EngagementSession(ndb.Model):

    actions = ndb.StructuredProperty(EngageAction,'a', repeated=True)
    cleaned = ndb.BooleanProperty('c', default=False)
    realized_at = ndb.DateTimeProperty('r', auto_now_add=True)
    audience_key = ndb.KeyProperty('k')

    _audience = None

    def get_audience(self):

        if not self._audience:
            self._audience = self.audience_key.get()

        return self._audience

class ExtensionTicket(ndb.Model):
    """
        Siempre descendiente de User.
    """

    origin = ndb.StringProperty('o', choices=('pay', 'admin', 'new_user'))
    long = ndb.IntegerProperty('l') # Longitud de la extensión en días
    created_by = ndb.KeyProperty('u') # En caso de que haya sido creado por un usuario administrador
    created_at = ndb.DateTimeProperty('a', auto_now_add=True)
    twitter_account = ndb.KeyProperty('t')
    pay_order = ndb.KeyProperty('p')
    consumed = ndb.BooleanProperty('c', default=False)
    consumed_at = ndb.DateTimeProperty('m')
    owner = ndb.KeyProperty('w') # Propietario del ticket. DEPRECATED?

    _twitter_account = None
    _pay_order = None

    def get_twitter_account(self):

        if not self._twitter_account:
            self._twitter_account = self.twitter_account.get()

        return self._twitter_account

    def apply_extension(self, twitter_account=None):

        if not twitter_account:
            twitter_account = self.twitter_account.get()
        else:
            self.twitter_account = twitter_account.key

        today = date.today()

        if twitter_account.due_date is not None and twitter_account.due_date > today :

            new_due_date = twitter_account.due_date + timedelta(days=self.long)
        else:
            new_due_date = today + timedelta(days=self.long)

        twitter_account.due_date = new_due_date

        self.consumed = True
        self.consumed_at = datetime.now()

        ndb.put_multi([self, twitter_account])

    def get_payment(self):
        if self._pay_order is None:
            if self.pay_order:
                self._pay_order = self.pay_order.get()
            
        return self._pay_order


class PaymentOrder(ndb.Model):

    processor = ndb.StringProperty('p', choices=('mercadopago', 'paypal', 'social miner'))
    currency = ndb.StringProperty('cy')
    #consolidated = ndb.BooleanProperty('r', default=False)
    consolidated_at = ndb.DateTimeProperty('ca')
    #payment_method = ndb.StringProperty('t', choices=('credit_card', 'transfer'))
    mount = ndb.FloatProperty('m')
    created_at = ndb.DateTimeProperty('c', auto_now_add=True)
    payer = ndb.KeyProperty('y')

    mp_preference_id = ndb.StringProperty('mpp')
    mp_payment_id = ndb.StringProperty('mppi')

    pp_payment_id = ndb.StringProperty('pppi')
    pp_transaction_id = ndb.StringProperty('ppti')

    _ticket = None

    def get_ticket(self):

        if not self._ticket:
            self._ticket = ExtensionTicket.query(ExtensionTicket.pay_order == self.key).get()

        return self._ticket

    @classmethod
    def consolidate(cls, provider_response, processor):
        """
            @return User key that paid if the pay was consolidated else None
        """


        payment_order = None

        if processor == 'mercadopago':
            payment_order = cls.consolidate_mercadopago(provider_response)
        elif processor == 'paypal':
            payment_order = cls.consolidate_paypal(provider_response)

        if payment_order and payment_order.consolidated_at:
            # We apply the extension ticket
            eticket = ExtensionTicket.query(ExtensionTicket.pay_order == payment_order.key).get()
            eticket.apply_extension()

            payment_order.put()

            return payment_order

        return None


    @classmethod
    def consolidate_mercadopago(self, provider_response):
        """
            @return the payment order model if was consolidate else return None
        """

        response = provider_response['response']

        if response['status'] == 'approved':

            payment_id = int(response['external_reference'])
            payment_order = ndb.Key(PaymentOrder, payment_id).get()

            if payment_order and not payment_order.consolidated_at:
                payment_order.consolidated = True
                payment_order.consolidated_at = datetime.now()
                payment_order.mp_payment_id = str(response['id'])

                return payment_order

        return None

    @classmethod
    def consolidate_paypal(self, provider_response):
        """
            @return the payment order model if was consolidate else return None
        """        

        payment_status = provider_response.get('payment_status')

        if payment_status == 'Completed':
            payment_id = int(provider_response.get('custom'))

            payment_order = ndb.Key(PaymentOrder, payment_id).get()

            if payment_order and not payment_order.consolidated_at:
                payment_order.consolidated = True
                payment_order.consolidated_at = datetime.now()
                payment_order.pp_transaction_id = str(provider_response.get('txn_id'))
                
                return payment_order

        return None