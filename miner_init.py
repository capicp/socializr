'''
Created on 08/01/2016

@author: capi
'''
import sys

if 'miner' not in sys.path:
    sys.path[0:0] = ['miner']

from google.appengine.ext import webapp

from routes import ROUTES


app = webapp.WSGIApplication(ROUTES, debug=True)
